﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminPage.aspx.cs" Inherits="Blog_Builder__BB_Api_.Admin.adminPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
		<title>Title</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="/js/jquery.min.js"></script>
		<script src="/js/skel.min.js"></script>
		<script src="/js/skel-layers.min.js"></script>
		<script src="/js/init.js"></script>
		<link rel="stylesheet" href="/css/skel.css" />
		<link rel="stylesheet" href="/css/style.css" />
		<link rel="stylesheet" href="/css/style-xlarge.css" />
		
</head>
<body>
    <header id="header" class="skel-layers-fixed">
				<h1><a href="#">Blog builder</a></h1>
				<nav id="nav">
					<ul>
						<li><a href="adminPage.aspx">Home</a></li>
						<li><a href="adminTheme.aspx">Theme</a></li>
                        <li><a href="adminPageEditor.aspx">Pages</a></li>
						<li><a href="/index.aspx" target="_blank">View Changes</a></li>
						<li><a href="admin.aspx?input=logout" class="button special">Log Out</a></li>
					</ul>
				</nav>
			</header>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
