﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admin.aspx.cs" Inherits="Blog_Builder__BB_Api_.Admin.admin" %>

<!--The designer will not display Style/Style.css/body/background.class-->

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
		<title>Title</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="/js/jquery.min.js"></script>
		<script src="/js/skel.min.js"></script>
		<script src="/js/skel-layers.min.js"></script>
		<script src="/js/init.js"></script>
		<link rel="stylesheet" href="/css/skel.css" />
		<link rel="stylesheet" href="/css/style.css" />
		<link rel="stylesheet" href="/css/style-xlarge.css" />
		
</head>
<body>
    <asp:Label ID="lbl1" runat="server" EnableTheming="True" Font-Bold="True" ForeColor="Red" Text="Wrong Username Or Password!" Visible="False"></asp:Label>
    <form id="form1" runat="server">
    <div>
    
        Username:<br />
        <asp:TextBox ID="txtUser" runat="server"></asp:TextBox>
        <br />
        Password:<br />
        <asp:TextBox ID="txtPass" runat="server" TextMode="Password"></asp:TextBox>
    
    </div>
        <p>
            <asp:Button ID="Button1" CssClass="loginButton" runat="server" Text="Button" OnClick="Button1_Click" BackColor="#FF6600" ForeColor="White" />
        </p> 
    </form>
    Cookies Have To Be Enabled To Use The Console!
</body>
</html>
